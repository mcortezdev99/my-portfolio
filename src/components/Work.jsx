import React from 'react'
import ImageGallery from 'react-image-gallery'

const images = [
    {
        original: 'https://live.staticflickr.com/65535/51414460699_d759a965a5_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413975383_6e29a197bb_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51412963917_fb5011e9d6_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413707641_3561d48fef_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413975333_20c6ef3358_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413707576_bbb35df686_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413975538_68593aa201_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414461579_59b24b7966_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51412964832_17f01df88d_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414691695_1850c6299f_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51412964357_0bbb3e133f_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51412964327_5654a455d1_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414691330_37c8395af3_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414461044_0fa790ce2c_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413975778_fb5685ac1c_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413708046_e135a8eb4d_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414691270_5b1977f3b6_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51412964272_09e939b0ed_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414460964_a2b289234c_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51413975668_15fd7fe2e4_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51414460909_03fffb7c9d_b.jpg',
    },
    {
        original: 'https://live.staticflickr.com/65535/51412964157_5ab1aa7bf2_b.jpg',
    },
  ];

const Work = () => {
  return (
    <div name='work' className='w-full md:h-screen text-gray-300 bg-[#0a192f]'>
       {/* Container */}
        <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
            <div className='pb-8'>
                <p className='text-4xl font-bold inline border-b-4 text-gray-300 border-pink-600'>Work</p>
                <p className='py-6'>// Check out some of my photography work below!</p>
            </div>
            <ImageGallery items={images}/>
        </div>
    </div>
  )
}

export default Work